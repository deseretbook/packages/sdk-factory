import jestMockFetch from 'jest-fetch-mock'

// Mock fetch
// https://www.npmjs.com/package/jest-fetch-mock
jestMockFetch.enableMocks()
