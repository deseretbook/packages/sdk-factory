import crossFetch from 'cross-fetch'
import URI from 'urijs'
import merge from 'deepmerge'
import queryString from 'qs'
import getDeepProperty from 'lodash.get'

const VALID_REST_METHODS = Object.freeze([
  'GET',
  'POST',
  'PATCH',
  'PUT',
  'DELETE',
])

// Sets the global fetch object to use the cross-fetch ponyfill
// https://www.npmjs.com/package/cross-fetch
global.fetch = crossFetch

/**
 * Defines a parameter as being required. If this function is called, it means a required parameter
 * wasn't provided.
 *
 * @param {string} name - Name of param that is missing.
 * @throws {Error}
 */
const requiredParam = name => {
  throw new Error(`"${name}" is required`)
}

/**
 * Removes empty properties from an object. Empty being defined as "null" or "undefined".
 *
 * @param {object} object - Object to remove properties from
 * @returns {object} Cleaned object
 */
const removeEmptyProperties = (object = {}) => {
  const newObject = Object.assign({}, object)

  Object.keys(object).forEach(key => {
    if (
      key in newObject &&
      (newObject[key] === null || newObject[key] === undefined)
    ) {
      delete newObject[key]
    }
  })

  return newObject
}

/**
 * Relevant endpoint and request data given to all resolvers.
 * @typedef {Object} ResolverData
 * @property {object} context - Current request context data.
 * @property {object} endpoint - Endpoint data used to build the current request.
 * @property {object} params - Data passed to the request.
 * @property {object} settings - SDK / request settings
 */

/**
 * Default sdk settings.
 */
const defaultSettings = {
  endpointResolver: {
    /**
     * Defines a resolver that returns what the endpoint values should be.
     *
     * @param {ResolverData} data - Context data for resolver
     * @returns {function} Function that returns the correct endpoint values.
     */
    resolveEndpoint(data) {
      return endpoint => endpoint || data.endpoint
    },
    /**
     * Resolves the correct
     * @param {*} data
     */
    resolve(data) {
      return this.resolveEndpoint(data)(data.endpoint)
    },
  },
  paramsResolver: {
    /**
     * Formats request data before it's sent to the API.
     *
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} - Function that accepts params and should return the formatted params.
     */
    formatParams() {
      return params => params
    },

    /**
     * Param resolver. This is where any changes or manipulations to the params should be made.
     *
     * @param {ResolverData} data - Relevant data that can be used to resolve params.
     * @returns {function} - Function that accepts params and should return the resolved params.
     */
    resolveParams(data) {
      return params => params || data.params
    },
    /**
     * Resolves parameters. This method handles all the resolving and formatting of request
     * parameters.
     *
     * @param {ResolverData} data - Relevant data that can be used to resolve params.
     * @returns {object} formatted params
     */
    resolve(data) {
      return this.formatParams(data)(this.resolveParams(data)(data.params))
    },
  },
  contextResolver: {
    /**
     * Context formatter
     *
     * @param {ResolverData} data - Relevant data that can be used in the formatter.
     * @returns {function} - Function that accepts context and should return the formatted context.
     */
    formatContext() {
      return context => context
    },
    /**
     * Context resolver
     *
     * @param {ResolverData} data - Relevant data that can be used in the resolver.
     * @returns {function} - Function that accepts context and should return the resolved context.
     */
    resolveContext() {
      return context => context
    },
    /**
     * Context resolver
     *
     * @param {ResolverData} data - Relevant data that can be used in the resolver.
     * @returns {function} - Function that accepts params and should return the resolved context.
     */
    resolve(data) {
      return this.formatContext(data)(this.resolveContext(data)(data.context))
    },
  },
  pathResolver: {
    queryStringSettings: {
      encode: false,
      arrayFormat: 'brackets',
    },
    variableRegex: /\/:([a-z._]*)/gi, // Begin with ':' and contains camel_case text with "." delimiters
    variableDelimiter: '.',

    /**
     * Resolves variable
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Variable resolver
     */
    resolveVariable({ context, params, endpoint }) {
      return variable => {
        const splitVariable = variable
          .split(this.variableDelimiter)
          .filter(Boolean)
        let value = getDeepProperty(context, splitVariable)

        if (
          (value === null || value === undefined) &&
          endpoint.context in context
        ) {
          value = getDeepProperty(context[endpoint.context], splitVariable)
        }

        if (value === null || value === undefined) {
          value = getDeepProperty(params, splitVariable)
        }

        return value
      }
    },

    /**
     * Query String Resolver
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Query String resolver
     */
    resolveQueryString({ endpoint, params }) {
      return path => {
        if (
          !endpoint ||
          !endpoint.method ||
          endpoint.method.toUpperCase() !== 'GET'
        ) {
          return path
        }

        return String(
          new URI(path).search(
            queryString.stringify(params, this.queryStringSettings),
          ),
        )
      }
    },

    /**
     * Resolves path
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Path resolver
     */
    resolvePath() {
      return path => path
    },
    /**
     * Resolves the path
     * @param {*} data - stuff
     * @returns {*} Resolved Path
     */
    resolve(data) {
      let { path } = data.endpoint

      path = path.replace(
        this.variableRegex,
        (_, variable) => `/${this.resolveVariable(data)(variable)}`,
      )

      let uri = new URI(this.resolveQueryString(data)(path))

      if (uri.is('relative') && data.settings.href) {
        uri = new URI(`${data.settings.href}${String(uri)}`)
      }

      return this.resolvePath(data)(String(uri))
    },
  },
  requestResolver: {
    /**
     * Resolves request headers before they're sent to the API.
     *
     * @param {ResolverData} data - Relevant data that can be used in the resolver.
     * @returns {function} Function that accepts headers and returns the actual headers
     */
    resolveHeaders({ settings }) {
      return headers => headers || settings.headers || {}
    },

    /**
     * Resolves request method before it's sent to the API.
     *
     * @param {ResolverData} data - Relevant data that can be used in the resolver.
     * @returns {function} Function that accepts the method and returns the actual method.
     */
    resolveMethod({ endpoint }) {
      return method => method || endpoint.method
    },

    /**
     * Formats the body.
     * @param {ResolverData} data - Relevant data that can be used in the formatter.
     * @returns {function} Function that takes the body and formats it into a string to be sent.
     */
    formatBody() {
      return body => JSON.stringify(body)
    },

    /**
     * Resolves request method before it's sent to the API.
     *
     * @param {ResolverData} data - Relevant data that can be used in the resolver.
     * @returns {function} Function that accepts the method and returns the actual method.
     */
    resolveBody(data) {
      return body => {
        if (this.resolveMethod(data)().toUpperCase() === 'GET') {
          return undefined
        }

        return this.formatBody(data)(body || data.params || {})
      }
    },

    /**
     * Resolves request settings
     *
     * @param {ResolverData} data - Relevant data that can be used in the resolver.
     * @returns {function} Function that resolves request settings
     */
    resolveSettings() {
      return settings => settings
    },

    /**
     * Resolves the request to be made to the API.
     *
     * @param {ResolverData} data - Relevant data that can be used in the resolver.
     * @returns {function} Function that resolves the request data.
     */
    resolve(data) {
      // https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch
      const extraSettings = [
        'mode',
        'credentials',
        'cache',
        'redirect',
        'referrer',
        'referrerPolicy',
        'integrity',
        'keepalive',
        'signal',
      ].reduce((settings, key) => {
        if (key in data.settings) {
          return {
            ...settings,
            [key]: data.settings[key],
          }
        }

        return settings
      }, {})

      return this.resolveSettings(data)(
        removeEmptyProperties({
          method: this.resolveMethod(data)().toUpperCase(),
          headers: removeEmptyProperties(this.resolveHeaders(data)()),
          body: this.resolveBody(data)(),
          ...extraSettings,
        }),
      )
    },
  },
  responseResolver: {
    /**
     * Stream Parser, attempts to format a response.body.
     * If there is no response body we will return without parsing.
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Stream parser
     */
    parseStream() {
      return responseStream =>
        responseStream.text().then(text => {
          try {
            return JSON.parse(text)
          } catch (error) {
            return text
          }
        })
    },

    /**
     * Checks errors
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Error checker
     */
    checkErrors() {
      return (responseData, response) => true
    },

    /**
     * Formats response
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Response formatter
     */
    formatResponse() {
      return responseData => responseData
    },

    /**
     * Response resolver
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Response resolver
     */
    resolveResponse() {
      return responseData => responseData
    },

    /**
     * Response resolver
     * @param {ResolverData} data - Relevant data that can be used to format params.
     * @returns {function} Response resolver
     */
    resolve(data) {
      return async response => {
        const responseData = await this.parseStream(data)(response)

        this.checkErrors(data)(responseData, response)

        return this.formatResponse(data)(
          this.resolveResponse(data)(responseData, response),
          response,
        )
      }
    },
  },
}

/**
 * Base SDK class. This class is used to define each module of the sdk.
 */
class SdkClass {
  /**
   * Class constructor
   *
   * @param {string} name - Module name
   * @param {object} [endpoints={}] - endpoints inside module
   * @param {object} [context={}] - Parent module context values
   * @param {object} [settings={}] - module settings
   */
  constructor(
    name = requiredParam('name'),
    endpoints = {},
    context = {},
    settings = {},
  ) {
    this.__name = name
    this.__endpoints = endpoints
    this.__settings = settings
    this.__context = context

    Object.entries(this.__endpoints).forEach(([endpointName, endpoint]) => {
      // Add extra data to the endpoint
      const updatedEndpoint = { ...endpoint, name: endpointName, context: name }

      // See if the endpoint is really a module.
      if (
        updatedEndpoint.endpoints &&
        Object.keys(updatedEndpoint.endpoints).length
      ) {
        this[updatedEndpoint.name] = (childContext = {}, childSettings = {}) =>
          new SdkClass(
            updatedEndpoint.name,
            updatedEndpoint.endpoints,
            { ...this.__context, [updatedEndpoint.name]: childContext },
            merge.all(
              [this.__settings, updatedEndpoint.settings, childSettings].filter(
                Boolean,
              ),
            ),
          )

        return
      }

      if (!updatedEndpoint.path || !updatedEndpoint.method) {
        console.warn(
          `"${updatedEndpoint.name}" Endpoint skipped. "path" and "method" are required when formatting an endpoint.`,
        )

        return
      }

      const requestMethod = updatedEndpoint.method.toUpperCase()

      if (!VALID_REST_METHODS.includes(requestMethod)) {
        console.warn(
          [
            `"${updatedEndpoint.name}" Endpoint skipped. Invalid request method provided.`,
            `Received: ${requestMethod}`,
            `Expected: ${VALID_REST_METHODS.join(', ')}`,
          ].join('\n'),
        )

        return
      }

      this[updatedEndpoint.name] = this.__request.bind(this, updatedEndpoint)
    })
  }

  /**
   * Generic request handler. Performs a fetch based on the given endpoint.
   *
   * @param {object} __endpoint - Endpoint schema data
   * @param {object} [params={}] - Request data
   * @param {object} [settings={}] - Request settings
   * @returns {Promise<object>} - Fetch Response promise.
   */
  __request(__endpoint, params = {}, settings = {}) {
    const requestSettings = merge.all(
      [this.__settings, __endpoint.settings, settings].filter(Boolean),
    )

    /**
     * merge.all() is not able to merge signal, it will return
     * it as an empty object. Object.assign will overwrite the
     * empty signal object with the correct information.
     */
    if (!!settings.signal) {
      Object.assign(requestSettings, { signal: settings.signal })
    }

    // Initial data we'll pass to every resolver.
    const resolverData = {
      params,
      endpoint: __endpoint,
      settings: requestSettings,
      context: this.__context,
    }

    // Resolve the resolver data.
    resolverData.params = requestSettings.paramsResolver.resolve(resolverData)
    resolverData.context = requestSettings.contextResolver.resolve(resolverData)
    resolverData.endpoint = requestSettings.endpointResolver.resolve(
      resolverData,
    )

    return fetch(
      requestSettings.pathResolver.resolve(resolverData),
      requestSettings.requestResolver.resolve(resolverData),
    ).then(requestSettings.responseResolver.resolve(resolverData))
  }
}

/**
 * Creates an SDK from the given schema
 *
 * @param {object} schema - API schema object
 * @returns {function} - new sdk based on the schema
 */
export function createSdk({
  name = requiredParam('schema.name'),
  endpoints = {},
  plugins = [],
  ...sdkSettings
} = {}) {
  return function({
    endpoints: instanceEndpoints = {},
    plugins: instancePlugins = [],
    ...instanceSettings
  } = {}) {
    return new SdkClass(
      name,
      merge.all([
        endpoints,
        ...plugins.map(({ endpoints }) => endpoints).filter(Boolean),
        instanceEndpoints,
        ...instancePlugins.map(({ endpoints }) => endpoints).filter(Boolean),
      ]),
      {},
      merge.all(
        [defaultSettings, sdkSettings, instanceSettings].filter(Boolean),
      ),
    )
  }
}
