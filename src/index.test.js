import { createSdk } from './'
import jestMockFetch from 'jest-fetch-mock'

// Sets fetch global object for mocking
global.fetch = jestMockFetch

const exampleSchema = {
  name: 'api name',
  href: 'https://www.app.example',
  endpoints: {
    user: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/users',
        },
        get: {
          method: 'GET',
          path: '/api/users/:id',
        },
        update: {
          method: 'PUT',
          path: '/api/users/:id',
        },
        patch: {
          method: 'PATCH',
          path: '/api/users/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/users/:id',
        },
        create: {
          method: 'POST',
          path: '/api/users',
        },
      },
    },
  },
}

describe('createSdk', () => {
  describe('constructor', () => {
    it('should throw an error if the schema is missing', () => {
      expect(() => {
        createSdk()
      }).toThrowError()
    })

    it('should return a class that can be instantiated', () => {
      const TestSdk = createSdk(exampleSchema)
      const instance = new TestSdk({ foo: 'bar' })

      expect(typeof TestSdk).toBe('function')
      expect(instance instanceof Object).toBe(true)
    })

    it('should createSdk a sub module properly', () => {
      const TestSdk = createSdk(exampleSchema)
      const instance = new TestSdk({ foo: 'bar' })

      expect(typeof instance.user).toBe('function')

      const userEndpoints = instance.user(
        { id: '4' },
        { headers: { foo: 'bar' } },
      )

      // Check to make sure all the endpoints were added.
      const expectedEndpoints = [
        'all',
        'get',
        'update',
        'delete',
        'create',
        'patch',
      ]

      expectedEndpoints.forEach(methodName => {
        expect(typeof userEndpoints[methodName]).toBe('function')
      })

      // Check the context to see if it was saved as expected
      expect(userEndpoints.__context).toHaveProperty('user', { id: '4' })

      // Check to see if settings override as expected.
      expect(userEndpoints.__settings).toHaveProperty('headers', { foo: 'bar' })
    })
  })

  describe('requests', () => {
    let endpoints
    const userContext = { id: '4' }
    const headers = { foo: 'bar' }

    beforeEach(() => {
      fetch.resetMocks()

      const TestSdk = createSdk(exampleSchema)
      const instance = new TestSdk()

      endpoints = instance.user(userContext, { headers })
    })

    describe('GET', () => {
      it('should make a GET request as expected with constructor params', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.get()

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual('https://www.app.example/api/users/4')
        expect(request.method).toEqual('GET')
        expect(request.headers).toHaveProperty('foo', 'bar')
      })

      it('should make a GET request as expected with method params', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.get(
          { id: 5, name: 'joel' },
          { headers: { color: 'purple' } },
        )

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual(
          'https://www.app.example/api/users/4?id=5&name=joel',
        )
        expect(request.method).toEqual('GET')
        expect(request.headers).toHaveProperty('foo', 'bar')
        expect(request.headers).toHaveProperty('color', 'purple')
      })
    })

    describe('POST', () => {
      it('should make a POST request as expected', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.create({ id: 1, name: 'joel' })

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual('https://www.app.example/api/users')
        expect(request.method).toEqual('POST')
        expect(request.headers).toHaveProperty('foo', 'bar')

        const body = JSON.parse(request.body)

        expect(body).toHaveProperty('id', 1)
        expect(body).toHaveProperty('name', 'joel')
      })
    })

    describe('PUT', () => {
      it('should make a PUT request as expected', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.update({ id: 1, name: 'joel' })

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual('https://www.app.example/api/users/4')
        expect(request.method).toEqual('PUT')
        expect(request.headers).toHaveProperty('foo', 'bar')

        const body = JSON.parse(request.body)

        expect(body).toHaveProperty('id', 1)
        expect(body).toHaveProperty('name', 'joel')
      })
    })

    describe('PATCH', () => {
      it('should make a PATCH request as expected', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.patch({ id: 1, name: 'joel' })

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual('https://www.app.example/api/users/4')
        expect(request.method).toEqual('PATCH')
        expect(request.headers).toHaveProperty('foo', 'bar')

        const body = JSON.parse(request.body)

        expect(body).toHaveProperty('id', 1)
        expect(body).toHaveProperty('name', 'joel')
      })
    })

    describe('DELETE', () => {
      it('should make a DELETE request as expected', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.delete({ id: 1, name: 'joel' })

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual('https://www.app.example/api/users/4')
        expect(request.method).toEqual('DELETE')
        expect(request.headers).toHaveProperty('foo', 'bar')

        const body = JSON.parse(request.body)

        expect(body).toHaveProperty('id', 1)
        expect(body).toHaveProperty('name', 'joel')
      })
      it('should succeed when the response is empty', async () => {
        fetch.mockResponseOnce()

        const response = await endpoints.delete({ id: 1, name: 'joel' })

        expect(response).toBe('')
      })
    })
  })

  describe('plugins', () => {
    let endpoints
    const userContext = { id: '4' }
    const headers = { foo: 'bar' }
    const plugin = {
      name: 'example plugin',
      endpoints: {
        user: {
          endpoints: {
            favoriteColor: {
              method: 'GET',
              path: '/api/users/:id/favorite_color',
            },
          },
        },
      },
    }

    describe('Schema Plugins', () => {
      beforeEach(() => {
        fetch.resetMocks()

        const TestSdk = createSdk({ ...exampleSchema, plugins: [plugin] })
        const instance = new TestSdk({ headers })

        endpoints = instance.user(userContext)
      })

      it('should add an extra request option', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.favoriteColor()

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual(
          'https://www.app.example/api/users/4/favorite_color',
        )
        expect(request.method).toEqual('GET')
        expect(request.headers).toHaveProperty('foo', 'bar')
      })
    })

    describe('Instance Plugins', () => {
      beforeEach(() => {
        fetch.resetMocks()

        const TestSdk = createSdk(exampleSchema)
        const instance = new TestSdk({ plugins: [plugin], headers })

        endpoints = instance.user(userContext)
      })

      it('should add an extra request option', async () => {
        fetch.mockResponseOnce('{}')

        await endpoints.favoriteColor()

        const [url, request] = fetch.mock.calls[0]

        expect(url).toEqual(
          'https://www.app.example/api/users/4/favorite_color',
        )
        expect(request.method).toEqual('GET')
        expect(request.headers).toHaveProperty('foo', 'bar')
      })
    })
  })
})
