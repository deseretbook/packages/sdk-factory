# 🏭 sdk-factory

Quickly build a JavaScript SDK to interface with any REST API.

## Installation

`yarn add sdk-factory`

## Usage

```
import { createSdk } from 'sdk-factory'

// Define the API schema
const schema = {
  name: 'Example API',
  endpoints: {
    user: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/users',
        },
        get: {
          method: 'GET',
          path: '/api/users/:id',
        },
        update: {
          method: 'PUT',
          path: '/api/users/:id',
        },
        patch: {
          method: 'PATCH',
          path: '/api/users/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/users/:id',
        },
        create: {
          method: 'POST',
          path: '/api/users',
        },
      },
    },
  },
}

// Create the SDK class.
const ExampleSdk = createSdk(exampleSchema)

// Create a new instance of the sdk to use it.
const instance = new ExampleSdk({ href: 'https://localhost:3000' })

instance.user().all()
  .then(users => console.log('Response from API: ', users))
```
